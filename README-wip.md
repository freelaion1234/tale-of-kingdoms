# Tale of Kingdoms: A new Conquest
*Tale of Kingdoms: A new Conquest* is a revival of the popular Tale of Kingdoms mod. We are based on the original 1.5.2, not any other remakes.
Contributors are very much welcome - and encouraged - to help.

## Useful links
** Website: ** https://www.islandearth.net/taleofkingdoms
** Discord: ** https://discord.gg/fh62mxU